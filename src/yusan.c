#define PROJECT_NAME "yusan"
#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <stdio.h>
#include <wayland-server.h>

#include "log.h"
#include "xdg-shell-protocol.h"

int main(int argc, char **argv) {
	if (argc != 1) {
		printf("%s takes no arguments.\n", argv[0]);
		return 1;
	}
	printf("This is project %s.\n", PROJECT_NAME);

	struct wl_display *display = wl_display_create();
	assert(display);

	const char *socket = wl_display_add_socket_auto(display);
	assert(socket);
	printf("Running Wayland compositor on socket %s\n", socket);

	return 0;
}
